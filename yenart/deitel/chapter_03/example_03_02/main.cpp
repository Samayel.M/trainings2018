#include <iostream>

class Layer
{
public:
    void
    show(int number, std::string name)
    {
        std::cout << "Layer " << number << ": " << name << std::endl;
    }
};

int
main()
{
    int layerNumber = 1;
    std::string layerName = "M1";

    Layer layer;

    layer.show(layerNumber, layerName);

    return 0;
}

