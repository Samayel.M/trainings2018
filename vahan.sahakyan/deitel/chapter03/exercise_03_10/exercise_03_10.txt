Classes often provide public member functions to allow clients of the class to set (i.e., assign values to) or get (i.e., obtain the values of) private data members.
This is done for "safety" of the objects, and also to keep away the client from the class's implementation (to keep it private).

In other words:
The SET function is to "make sure" the data that is to be placed in the object is not wrong.
The GET function accesses the value of a data member, not letting the client to interact with the data member directly.
We can say that these functions literally hide the internal data representation.
