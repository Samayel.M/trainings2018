///Program that displays the sum, product, 
///difference, and quotient of given two numbers.

#include <iostream> ///allows the program to input and output

///function main begins program execution

int
main() 
{
    /// variable declarations
    int number1, number2; /// two numbers that will be used in calculations
 
    std::cout << "Enter the first number: "; ///prompt user for data
    std::cin >> number1; ///read the first number into number1

    std::cout << "Enter the second number: "; /// prompt user for data
    std::cin >> number2; ///read the second number into number1

    std::cout << "Sum is " << number1 + number2 << std::endl; /// add the numbers and display the sum
    std::cout << "Product is " << number1 * number2 << std::endl; /// multiply the numbers and display the product
    std::cout << "Difference is " << number1 - number2  << std::endl; /// subtract number2 from number1 and  display the difference
    
    if (0 == number2) {    
        std::cout << "Error: cannot be divided by 0." << std::endl;
    
	return 1;
    }

    std::cout << "Quotient is " << number1 / number2 << std::endl; /// divide number1 by number2 and  display the  quotient    
    
    return 0; /// indicate that program ended successfully
} /// end function main

