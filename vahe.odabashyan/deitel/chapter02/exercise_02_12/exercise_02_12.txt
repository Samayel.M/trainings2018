What, if anything, prints when each of the following C++ statements is performed? If nothing prints, then answer "nothing." Assume x = 2 and y = 3. 
1.cout << x;     2
2.cout << x + x; 4  
3.cout << "x=";  x=
4.cout << "x = " << x;     x=2
5.cout << x + y << " = " << y + x;    5 = 5
6.z = x + y;     nothing
7.cin >> x >> y; nothing
8.// cout << "x + y = " << x + y; nothing
9.cout << "\n"; nothing
