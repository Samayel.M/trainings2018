/// A program that prints a box, an oval, an arrow and a diamond

#include <iostream> /// allows the program to input and output

/// function main begins program execution

int
main()
{
	std::cout << "*********         ***             *             *" << std::endl;
	std::cout << "*       *       *     *          ***           * *" << std::endl;
        std::cout << "*       *      *       *        *****         *   *" << std::endl;
        std::cout << "*       *      *       *          *          *     *" << std::endl;
        std::cout << "*       *      *       *          *         *       *" << std::endl;
        std::cout << "*       *      *       *          *          *     *" << std::endl;
        std::cout << "*       *      *       *          *           *   *" << std::endl;
	std::cout << "*       *       *     *           *            * *" << std::endl;
	std::cout << "*********         ***             *             *" << std::endl;

    return 0; /// indicates the successful completion of the program
} /// end of function main

