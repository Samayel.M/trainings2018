/// A program that detect whether the first number is a multiple of the secone one.

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    int number1, number2; /// 2 variables to store the provided numbers
    std::cout << "Enter 2 numbers: "; /// prompts for data
    std::cin >> number1 >> number2; /// reads 2 numbers
    
    if (0 == number2) {
        std::cout << "Error 1: the second number cannot be 0" << std::endl;
        return 0;
    }

    if (number1 % number2 == 0) {
        std::cout << number1 << " is the multiple of " << number2 << std::endl;
	return 0;
    }

    std::cout << number1 << " is not the multiple of " << number2 << std::endl;

    return 0; // the program ended successfully
} // end of function main

