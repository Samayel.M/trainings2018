<?php

namespace Controllers\Error;
use Libs\Controller;

///////////////////////////////////////////////////////////////////
// 
// class Error extends Controller
//
// Control Error pages.
//
// public function __construct()
// public function index()
//
///////////////////////////////////////////////////////////////////

class Error extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        self::view()->load('error/index', $this->getData());
    }
}
?>