<?php

namespace Models;
use Libs\Model;

///////////////////////////////////////////////////////////////////
// 
// class Menu extends Model
//
// Work with Menu table in database
//
// public function __construct()
// public function get($id = NULL)
// 
// private $Menu = [];
//
///////////////////////////////////////////////////////////////////

class Menu extends Model
{
    /**
     * The array of menu fields with links in db
     */
    private $menu_ = [];

    /**
     * The constructor calls parent constructor
     * and gets the connectivity db
     */
    function __construct()
    {
        // TODO: Save it in db
        $this->menu_ = [
            "welcome"   => "Lessons",
            "about"     => "About",
            "contact"   => "Contact",
            "faq"       => "FAQ'S",
            "exercises" => "Exercises"
        ];
        parent::__construct();
    }

    /**
     * Retruns menu from db
     *
     * @param int $id - the current id of menu field in db
     * @return array $this->menu_ - array of menus in db
     *         | current menu which id = $id | NULL
     */
    public function get($id = NULL)
    {
        if (is_null($id)) {
            return $this->menu_;
        }
        return NULL;
    }
}
?>
