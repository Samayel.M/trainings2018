<?php

namespace Libs;
use \Exception;

///////////////////////////////////////////////////////////////////
// 
// LIBRARY BOOTSTRAP
//
// All requests are handled from bootstrap library.
// As each page is a one function from controller Bootstrsp detects
// each page controller and call of current function from controller. 
// 
//
///////////////////////////////////////////////////////////////////

class Bootstrap
{
    private static $bootstrap = NULL;

    // TODO: add more functionality and service functions

    /**
     * Not allow the object creation
     */
    private function __construct()
    {
        $url = !empty($_GET['site']) ? $_GET['site'] : 'welcome';
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        try {
            $controllerName = "Controllers\\$url[0]";
            $controllerFileName = str_replace("\\", "/", $controllerName).'.php';
            if (!file_exists(strtolower($controllerFileName))) {
                throw new Exception("Error");
            }
            $controller = new $controllerName();
            if (isset($url[1])) {
                $url[2] = isset($url[2]) ? $url[2] : NULL;
                $controller->{$url[1]}($url[2]);
            } else {
                $controller->index();
            }
        } catch (Exception $e) {
            return $this->error();
        }
    }

    /**
     * Not allow the cloning of single object
     */
    private function __clone() {}

    public static function process()
    {
        if (is_null(self::$bootstrap)) {
            self::$bootstrap = new Bootstrap();
        }
        return self::$bootstrap;
    }

    private function error() {
        $controller = new \Controllers\Error\Error();
        $controller->index();
        return false;
    }
}
?>